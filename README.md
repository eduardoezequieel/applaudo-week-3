# applaudo-week-3-challenge

<img src="dist/img/1.png" alt="landing-page">
<img src="dist/img/3.png" alt="posts">
<img src="dist/img/4.png" alt="posts">
<img src="dist/img/5.png" alt="posts">
<img src="dist/img/2.png" alt="posts">

## About this repository

Simple application that emulates a news website, you can create your own posts and they are getting saved in your local storage (You can even filter them with title and tags too).

## The deployment of the application is available at https://applaudo-week-3-challenge.vercel.app/