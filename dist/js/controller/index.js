import IndexState from '../page/index-state.js';

const page = new IndexState();

document.addEventListener('DOMContentLoaded', () => {
  page.init();
});

document.querySelector('#nav-links').addEventListener('click', (e) => {
  if (e.target.id != 'home' && e.target.tagName == 'A') {
    page.filterState(e.target.id);
  } else if (e.target.id == 'home') {
    document.querySelector('.active-page').classList.remove('active-page');
    e.target.classList.add('active-page');
    page.change(page.initState());
  }
});

document.querySelector('#search').addEventListener('submit', (e) => {
  e.preventDefault();
  const input = document.querySelector('#searchPost');

  if (input.value.length != 0) {
    page.change(page.searchState(input.value));
  } else {
    page.change(page.initState());
  }
});
