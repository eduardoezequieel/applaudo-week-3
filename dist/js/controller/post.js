import PostState from '../page/post-state.js';

const page = new PostState();
const params = new URLSearchParams(location.search);

document.addEventListener('DOMContentLoaded', () => {
  page.init(params.get('id'));
});
