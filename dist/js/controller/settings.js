import SettingsState from '../page/settings-state.js';
import Validator from '../helpers/validator.js';
import SettingsService from '../services/settings.js';

const page = new SettingsState();
const validator = new Validator();
const service = new SettingsService();

document.addEventListener('DOMContentLoaded', () => {
  page.init();
});

document.querySelector('#add').addEventListener('click', () => {
  validator.clearForms();
  page.change(page.addState());
});

document.querySelector('#posts').addEventListener('click', () => {
  page.change(page.postsState());
});

document.querySelector('#posts-container').addEventListener('click', (e) => {
  if (e.target.getAttribute('mode') == 'edit') {
    page.change(page.editState(e.target.id));
  } else if (e.target.getAttribute('mode') == 'delete') {
    if (confirm('Are you sure that you want to delete this post?')) {
      service.deletePost(e.target.id);
      page.change(page.postsState());
    }
  }
});

document.querySelector('#txtTitle').addEventListener('keyup', (e) => {
  if (e.keyCode != 9 && e.keyCode != 16) {
    validator.validateAlphanumeric(e.target.id, 'Invalid title');
  }
});

document.querySelector('#txtTags').addEventListener('keyup', (e) => {
  if (e.keyCode != 9 && e.keyCode != 16) {
    validator.validateTags(e.target.id, 'Invalid tags');
  }
});

document.querySelector('#txtBody').addEventListener('keyup', (e) => {
  if (e.keyCode != 9 && e.keyCode != 16) {
    validator.validateParagraph(e.target.id, 'Invalid body');
  }
});

document.querySelector('#txtAutor').addEventListener('keyup', (e) => {
  if (e.keyCode != 9 && e.keyCode != 16) {
    validator.validateAlphabetic(e.target.id, 'Invalid name');
  }
});

document.querySelector('#txtImage').addEventListener('keyup', (e) => {
  if (e.keyCode != 9 && e.keyCode != 16) {
    validator.validateUrl(e.target.id, 'Invalid URL');
  }
});

document.querySelector('#manage-post').addEventListener('submit', (e) => {
  e.preventDefault();

  const errors = document.querySelectorAll('.error');
  const inputs = document.querySelectorAll("input[type='text'], textarea");
  let warning = false;

  errors.length != 0 ? (warning = true) : (warning = false);

  for (let i = 0; i < inputs.length; i++) {
    if (inputs[i].value.length == 0) {
      warning = true;
      break;
    }
  }

  if (!warning) {
    if (e.target.getAttribute('mode') == 'add') {
      service.savePost(
        document.querySelector('#txtTitle').value,
        document.querySelector('#txtTags').value,
        document.querySelector('#txtAutor').value,
        document.querySelector('#txtBody').value,
        document.querySelector('#txtImage').value
      );

      alert('Saved successfully!');

      validator.clearForms();

      page.change(page.postsState());
    } else if (e.target.getAttribute('mode') == 'edit') {
      if (document.querySelector('#txtId').value.length != 0) {
        service.editPost(
          document.querySelector('#txtId').value,
          document.querySelector('#txtTitle').value,
          document.querySelector('#txtTags').value,
          document.querySelector('#txtAutor').value,
          document.querySelector('#txtBody').value,
          document.querySelector('#txtImage').value
        );

        alert('Edited successfully!');

        page.change(page.postsState());
      }
    }
  } else {
    alert('Invalid data');
  }
});
