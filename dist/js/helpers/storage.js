export default class LocalStorage {
  constructor() {
    if (LocalStorage.instance instanceof LocalStorage) {
      return LocalStorage.instance;
    }

    this.posts = this.#getPosts();
    LocalStorage.instance = this;
  }

  #savePosts() {
    localStorage.setItem('posts', JSON.stringify(this.posts));
  }

  savePost(post) {
    this.posts.push(post);
    this.#savePosts();
  }

  getPost(id) {
    return this.posts[this.#searchId(id)];
  }

  editPost(post) {
    let i = this.#searchId(post.id);

    this.posts[i].title = post.title;
    this.posts[i].tags = post.tags;
    this.posts[i].autor = post.autor;
    this.posts[i].body = post.body;
    this.posts[i].url = post.url;

    this.#savePosts();
  }

  deletePost(id) {
    this.posts.splice(this.#searchId(id), 1);
    this.#savePosts();
  }

  #searchId(id) {
    let index = 0;

    for (let i = 0; i < this.posts.length; i++) {
      if (this.posts[i].id == id) {
        index = i;
        break;
      }
    }

    return index;
  }

  #getPosts() {
    const posts = JSON.parse(localStorage.getItem('posts'));
    return !posts ? [] : posts;
  }
}
