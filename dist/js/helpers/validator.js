export default class Validator {
  #validate(id, errorMessage, regex) {
    const input = document.getElementById(id);
    if (input.value.match(regex)) {
      input.style.border = '1px solid #04a87e';

      const errorLabel = document.querySelector(`#${input.id}Error`);
      if (errorLabel !== null) {
        errorLabel.remove();
      }
    } else {
      input.style.border = '1px solid red';

      if (document.querySelector(`#${input.id}Error`) === null) {
        const errorLabel = document.createElement('label');
        errorLabel.textContent = errorMessage;
        errorLabel.className = 'error';
        errorLabel.id = `${input.id}Error`;
        input.parentNode.appendChild(errorLabel);
      }
    }
  }

  validateAlphanumeric(id, errorMessage) {
    this.#validate(id, errorMessage, /^[a-z $ , 0-9]+$/i);
  }

  validateTags(id, errorMessage) {
    this.#validate(id, errorMessage, /^[A-Z a-z 1-9 ,]+$/);
  }

  validateAlphabetic(id, errorMessage) {
    this.#validate(id, errorMessage, /^[A-Z a-z]+$/);
  }

  validateParagraph(id, errorMessage) {
    this.#validate(id, errorMessage, /([A-Za-z\s])\w+/g);
  }

  validateUrl(id, errorMessage) {
    this.#validate(id, errorMessage, /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi);
  }

  clearForms() {
    const forms = document.querySelectorAll('form');
    const inputs = document.querySelectorAll('input, textarea');
    const errors = document.querySelectorAll('label.error');

    forms.forEach((element) => {
      element.reset();
    });

    inputs.forEach((element) => {
      element.style.border = '1px solid #CCCCCC';
    });

    errors.forEach((element) => {
      element.remove();
    });
  }

  clearPosts(){
    document.getElementById('')
  }
}
