import LocalStorage from '../helpers/storage.js';

export default class Post {
  #storage = new LocalStorage();

  constructor(title, tags, autor, body, url) {
    this.id = this.#generateId();
    this.title = title;
    this.tags = tags;
    this.autor = autor;
    this.body = body;
    this.url = url;
    this.date = this.#generateDate();
  }

  #generateId() {
    return this.#storage.posts.length > 0 ? parseInt(this.#storage.posts[this.#storage.posts.length - 1].id) + 1 : 1;
  }

  #generateDate() {
    const date = new Date();
    return `${date.getDate()} / ${date.getMonth() + 1} / ${date.getFullYear()}`;
  }
}
