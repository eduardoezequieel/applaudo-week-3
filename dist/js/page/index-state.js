import IndexService from '../services/index.js';

export default class IndexState {
  #service = new IndexService();

  constructor() {
    let currentState;

    this.init = () => this.change(this.initState());

    this.change = (state) => {
      currentState = state;
    };
  }

  initState() {
    const posts = this.#service.getPosts();

    this.renderPosts(posts.slice().reverse());

    let rows = '';
    let i = 0;
    this.#service.getTags().forEach((tag) => {
      rows += `
        <li><a value="${tag}" id="tag-${i}" href="#!">${tag.toUpperCase()}</a></li>
      `;
      i++;
    });

    document.querySelector('#tags').innerHTML = rows;
  }

  filterState(id) {
    const selectedTag = document.querySelector(`#${id}`);
    document.querySelector('.active-page').classList.remove('active-page');
    selectedTag.classList.add('active-page');

    this.renderPosts(this.#service.getPostsByTag(selectedTag.getAttribute('value')));
  }

  searchState(title) {
    const posts = this.#service.getPostsByTitle(title);

    if (posts.length > 0) {
      document.querySelector('#feature-section').classList.add('d-none');

      let rows = '';
      posts.forEach((post) => {
        rows += `<div class="regular-post">
        <div class="image-featured">
          <img src="${post.url}" alt="elon-musk" />
        </div>
        <div>
          <div class="heading">
            <a href="html/post.html?id=${post.id}">${post.title}</a>
          </div>
          <span>${post.body.substring(0, 200)}...</span>
          <div class="credits">
            <h5>${post.autor}</h5>
            <span>${post.date}</span>
          </div>
        </div>
      </div>`;
      });

      document.querySelector('#regular-section').innerHTML = rows;
    } else {
      alert('No results found');
    }
  }

  renderPosts(posts) {
    document.querySelector('#feature-section').classList.remove('d-none');
    let i = 0;
    let firstPost = '',
      secondPost = '',
      featuredPosts = '',
      regularPosts = '';

    if (posts.length > 0) {
      document.querySelector('.message').classList.add('d-none');
      posts.forEach((post) => {
        if (i == 0) {
          firstPost = `
            <div class="image-featured">
              <img src="${post.url}"/>
            </div>
            <div>
              <div class="heading">
                <a href="html/post.html?id=${post.id}">${post.title}</a>
              </div>
              <span>${post.body.substring(0, 130)}...</span>
              <div class="credits">
                <h5>${post.autor}</h5>
                <span>${post.date}</span>
              </div>
            </div>
            `;
        }

        if (i == 1) {
          secondPost = `
            <div class="image-featured">
              <img src="${post.url}"/>
            </div>
            <div>
              <div class="heading">
                <a href="html/post.html?id=${post.id}">${post.title}</a>
              </div>
              <span>${post.body.substring(0, 130)}...</span>
              <div class="credits">
                <h5>${post.autor}</h5>
                <span>${post.date}</span>
              </div>
            </div>
            `;
        }

        if (i > 1 && i < 5) {
          featuredPosts += `
              <div class="feature-post-small">
                <div class="image-featured">
                  <img src="${post.url}" />
                </div>
                <div>
                  <div class="heading">
                    <a href="html/post.html?id=${post.id}">${post.title}</a>
                  </div>
                  <span>${post.body.substring(0, 30)}...</span>
                  <div class="credits">
                    <h5>${post.autor}</h5>
                    <span>${post.date}</span>
                  </div>
                </div>
              </div>
              `;
        }

        if (i > 4) {
          regularPosts += `
            <div class="regular-post">
              <div class="image-featured">
                <img src="${post.url}" alt="elon-musk" />
              </div>
              <div>
                <div class="heading">
                  <a href="html/post.html?id=${post.id}">${post.title}</a>
                </div>
                <span>${post.body.substring(0, 200)}...</span>
                <div class="credits">
                  <h5>${post.autor}</h5>
                  <span>${post.date}</span>
                </div>
              </div>
            </div>
            `;
        }

        i++;
      });

      document.querySelector('#first-post').innerHTML = firstPost;
      document.querySelector('#second-post').innerHTML = secondPost;
      document.querySelector('#featured-posts').innerHTML = featuredPosts;
      document.querySelector('#regular-section').innerHTML = regularPosts;
    } else {
      document.querySelector('.message').classList.remove('d-none');
    }
  }
}
