import PostService from '../services/post.js';

export default class PostState {
  #service = new PostService();

  constructor() {
    let currentState;

    this.init = (id) => this.change(this.initState(id));

    this.change = (state) => {
      currentState = state;
    };
  }

  initState(id) {
    const post = this.#service.getPost(id);

    document.querySelector('#image').setAttribute('src', post.url);
    document.querySelector('#title').textContent = post.title;
    document.querySelector('#tags').textContent = post.tags.toString();
    document.querySelector('#date').textContent = post.date;
    document.querySelector('.data').innerHTML = `<p>${post.body}</p>`;
  }
}
