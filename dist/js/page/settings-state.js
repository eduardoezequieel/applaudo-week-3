import LocalStorage from '../helpers/storage.js';
import SettingsService from '../services/settings.js';
export default class SettingsState {
  #service = new SettingsService();

  constructor() {
    let currentState;

    this.init = () => this.change(this.postsState());

    this.change = (state) => {
      currentState = state;
    };
  }

  #changeTitle(title) {
    document.querySelector('#title').textContent = title;
  }

  #hideActualState() {
    const element = document.querySelector('.default');
    const button = document.querySelector('.active');

    if (element != null) {
      element.classList.remove('default');
      element.classList.add('d-none');
    }

    if (button != null) {
      button.classList.remove('active');
    }
  }

  postsState() {
    this.#changeTitle('Manage posts');
    this.#hideActualState();

    document.querySelector('#posts').classList.add('active');

    const container = document.querySelector('#posts-container');
    container.classList.remove('d-none');
    container.classList.add('default');

    let rows = '';

    const posts = new Object(this.#service.getPosts());
    if (posts.length > 0) {
      posts
        .slice()
        .reverse()
        .forEach((post) => {
          rows += `
      <div class="post-card">
        <div class="image-featured">
          <img src="${post.url}" alt="" />
          <h5>${post.title}</h5>
        </div>
        <div class="btn-group">
          <button id="${post.id}" mode="edit">Edit</button>
          <button id="${post.id}" mode="delete">Delete</button>
        </div>
      </div>
      `;
        });

      container.innerHTML = rows;
    } else {
      document.querySelector('.message').classList.remove('d-none');
    }
  }

  addState() {
    this.#hideActualState();
    if (!document.querySelector('.message').classList.contains('d-none')) {
      document.querySelector('.message').classList.add('d-none');
    }
    this.#changeTitle('Create post');

    document.querySelector('#add').classList.add('active');

    const form = document.querySelector('#manage-post');
    form.setAttribute('mode', 'add');
    form.classList.remove('d-none');
    form.classList.add('default');
  }

  editState(id) {
    this.#hideActualState();
    this.#changeTitle('Edit post');

    document.querySelector('#posts').classList.add('active');

    const form = document.querySelector('#manage-post');
    form.setAttribute('mode', 'edit');
    form.classList.remove('d-none');
    form.classList.add('default');

    const post = this.#service.getPost(id);

    document.querySelector('#txtId').value = post.id;
    document.querySelector('#txtTitle').value = post.title;
    document.querySelector('#txtTags').value = post.tags.toString();
    document.querySelector('#txtAutor').value = post.autor;
    document.querySelector('#txtBody').value = post.body;
    document.querySelector('#txtImage').value = post.url;
  }
}
