import LocalStorage from '../helpers/storage.js';

export default class IndexService {
  #storage = new LocalStorage();

  getPosts() {
    return this.#storage.posts;
  }

  getTags() {
    const tags = new Set();

    this.#storage.posts.forEach((post) => {
      post.tags.forEach((tag) => {
        tags.add(tag.trim());
      });
    });

    return tags;
  }

  getPostsByTag(tag) {
    const filteredPosts = [];
    let matches = false;

    this.#storage.posts.forEach((post) => {
      matches = false;

      for (let i = 0; i < post.tags.length; i++) {
        if (post.tags[i] == tag) {
          matches = true;
          break;
        }
      }

      if (matches) {
        filteredPosts.push(post);
      }
    });

    return filteredPosts.slice().reverse();
  }

  getPostsByTitle(title) {
    return this.#storage.posts
      .slice()
      .reverse()
      .filter((post) => {
        return post.title.toLowerCase().includes(title.toLowerCase());
      });
  }
}
