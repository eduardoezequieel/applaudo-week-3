import LocalStorage from '../helpers/storage.js';

export default class PostService {
  #storage = new LocalStorage();

  getPost(id){
    return this.#storage.getPost(id);
  }
}
