import LocalStorage from '../helpers/storage.js';
import Post from '../models/post.js';

export default class SettingsService {
  #storage = new LocalStorage();

  savePost(title, tags, autor, body, image) {
    const tagsArray = tags.split(',');

    let i = 0;
    tagsArray.forEach(tag => {
      tagsArray[i] = tag.trim();
      i++;
    });
    
    this.#storage.savePost(new Post(title, tagsArray, autor, body, image));
  }

  editPost(id, title, tags, autor, body, image) {
    const tagsArray = tags.split(',');

    let i = 0;
    tagsArray.forEach(tag => {
      tagsArray[i] = tag.trim();
      i++;
    });

    const post = {};

    post.id = id;
    post.title = title;
    post.tags = tagsArray;
    post.autor = autor;
    post.body = body;
    post.url = image;

    this.#storage.editPost(post);
  }

  deletePost(id) {
    this.#storage.deletePost(id);
  }

  getPosts() {
    return this.#storage.posts;
  }

  getPost(id) {
    return this.#storage.getPost(id);
  }
}
